const protocol = 'http';
const host = 'localhost:8000';

pm.environment.set('host', `${protocol}://${host}`);
pm.environment.set('hotel', 4687);

const apiUrlLogin = `${protocol}://${host}/api/v1/auth/login/`;

const authPayload = {
    email: 'test@example.ru',
    password: 'awesomePassword123'
}

const header = 'Content-Type:application/json';

const obtainTokenRequest = {
    method: 'POST',
    url: apiUrlLogin,
    header: header,
    body: JSON.stringify(authPayload)
}

const getCurrentTime = () => {
    return new Date().getTime();
}

const isTokenExpired = () => {
    return getCurrentTime() - 600000;
}

let sendRequest = true;
let accessToken = pm.environment.get('accessToken');
let accessTokenExpiry = pm.environment.get('accessTokenExpiry')

if (!accessToken || !accessTokenExpiry) {
    console.log('Access Token and its Expiry dates are missing.');
} else if (accessTokenExpiry <= isTokenExpired()) {
    console.log('Access Token expired.');
} else {
    sendRequest = false;
    console.log('Access Token is OK.');
}

if (sendRequest === true) {
    pm.sendRequest(obtainTokenRequest, (err, res) => {
        const responseJson = res.json();
        console.log(err ? err: responseJson);

        if (err === null && responseJson.code != 'token_not_valid') {
            accessToken = responseJson.access;
            console.log('Save Access Token and update Expiry dates.');

            pm.environment.set('accessToken', accessToken);
            pm.environment.set('accessTokenExpiry', getCurrentTime());
        } else {
            console.log('Something went wrong.');
        }
    })
}

